FROM nginx:alpine

WORKDIR /app

COPY ./files ./static

COPY ./nginx.conf /etc/nginx/nginx.conf 

EXPOSE 80