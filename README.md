# Introduction
This repository contains all css-variables which should be imported for all NUM-RPS projects.

An Ansible role has been written for deployment (see RPS GitLab project with NUM group vars)

Right now the css file is served via: 

https://style.rps.netzwerk-universitaetsmedizin.de/css-variables.css

# Todos:

General
- [ ] clean up not used variables

Netzwerk-Universitätsmedizin To-Dos:
- [ ] integrate into NUM Registration mask
- [ ] integrate into NUM Keycloak user self service
- [ ] integrate into NUM Login
- [ ] integrate into NUM Header
- [ ] integrate into NUM Foyer
- [ ] integrate into NUM Group Interface
- [ ] integrate into NUM Contact Mask
